import argparse
from testing.calc import add, multiply

parser = argparse.ArgumentParser()
parser.add_argument("-x", "--operand1")
parser.add_argument("-y", "--operand2")
parser.add_argument("-o", "--operation")
args = parser.parse_args()

if args.operation == "add":
    result = add(int(args.operand1), int(args.operand2))
    print(result)
elif args.operation == "mult":
    result = multiply(int(args.operand1), int(args.operand2))
    print(result)
